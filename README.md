![](https://gitlab.com/jstone28/triangle/uploads/69efb910977e506f6f727331d21eefed/Screen_Shot_2019-06-06_at_8.27.57_AM.png)

# triangle

The triangle excercise is a function that's meant to identify the triangle type based on inputs

## Getting Started

To run this application, clone the project and run:

`go test -v .`

That will kick off a set of tests that verify the functionality of `TriangleTester()`

*An output of this command can also be found in the pipelines (CI/CD on the left side navigation bar)*

## Problem

**Write a function that takes three sides of a triangle and answers if it is equilateral, isosceles, or scalene.**

## Definitions

**equalateral**: *an In geometry, an equilateral triangle is a triangle in which all three sides are equal. In the familiar Euclidean geometry, an equilateral triangle is also equiangular; that is, all three internal angles are also congruent to each other and are each 60°.* [source](https://en.wikipedia.org/wiki/Equilateral_triangle)

**isoceles**: *In geometry, an isosceles triangle is a triangle that has two sides of equal length. Sometimes it is specified as having exactly two sides of equal length, and sometimes as having at least two sides of equal length, the latter version thus including the equilateral triangle as a special case.* [source](https://en.wikipedia.org/wiki/Isosceles_triangle)

**scalene**: *A scalene triangle has all its sides of different lengths. Equivalently, it has all angles of different measure.* [source](https://en.wikipedia.org/wiki/Triangle#Types_of_triangle)

## Solution Overview

The most straightfoward implementation of this problem (the most readable) is a series of if statements that compare the values of each side. Based on the emergent pattern, the function returns what type of triangle is being passed in.

Go is pretty cool in that a lot of the peripheral error handling is done for you through types and basic constructs. For example, in JS/Node I'd have to write tests to cover null/nil and improper values being passed in but, here, the go compiler handles it.

*The assumptions made in this solution are expounded upon in the next section*


## Thoughts During Exercise

* My interpretation of "take 3 sides of a triangle" is that they are 3 seperate int values and not a single data struct type (like json, yaml, etc) with 3 sizes listed as attributes.

* In reading the definitions of equalateral and isoceles triangles, it seems like equalateral triangles are also (occassionally) listed within the definition of isoceles triangles. For this exercise, I am going to treat the 3 triangle types listed as completely seperate however there is a scenario where this function could return a concatenated string of both `equilateral and isoceles` (similar to the fizzbuzz problem)

* I'm not sure of the constraints on third-party library usage so I'm going to avoid it but using something like `stretchr/testify` or `onsi/gingko` may increase readability in the assert statements and test output

* The return type/structure wasn't exactly specified so I defaulted to a string that could be compared against another string in the test. This is probably a requirement I would ask for up front: is there any specific data type you're looking to get back(?)

* Finally, I wouldn't usually push directly on master but because this is a fairly straight forward and finite project :sunglasses:
