## 1.0.3 [06-06-2019]
* Bug fixes and performance improvements

See commit 6cac6ba

---

## 1.0.2 [06-06-2019]
* Bug fixes and performance improvements

See commit cadd46a

---

## 1.0.1 [06-06-2019]
* Bug fixes and performance improvements

See commit ee78e94

---
