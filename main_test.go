package main

import (
	"testing"
)

// assertEquals is a basic assertEquals implementation
func assertEquals(t *testing.T, foo interface{}, bar interface{}) {
	if foo != bar {
		t.Errorf("%s != %s", foo, bar)
	}
}

// TestTriangleTesterEquilateralTriangle tests that the TriangleTester function
// returns a string "equilateral" when the sides are equivalent
func TestTriangleTesterEquilateralTriangle(t *testing.T){
	triangle := TriangleTester(1, 1, 1)
	assertEquals(t, triangle, "equilateral")
}

// TestTriangleTesterIsoscelesTriangle tests that the TriangleTester function
// returns a string "isosceles" when two sides are equivalent
func TestTriangleTesterIsoscelesTriangle(t *testing.T){
	triangle := TriangleTester(1, 2, 2)
	assertEquals(t, triangle, "isosceles")
}

// TestTriangleTesterScaleneTriangle tests that the TriangleTester function
// returns a string "scalene" when no sides are equivalent
func TestTriangleTesterScaleneTriangle(t *testing.T){
	triangle := TriangleTester(1, 2, 3)
	assertEquals(t, triangle, "scalene")
}
