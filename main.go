package main

import(
	"fmt"
)

// TriangleTester takes in 3 sides of a triangle and determines triangle type
// TriangleTester returns a string describing the triangle type
func TriangleTester(side1 int, side2 int, side3 int) string {
	if side1 == side2 && side2 == side3 {
		return "equilateral"
	} else if side1 == side2 || side2 == side3 || side1 == side3 {
		return "isosceles"
	} else {
		return "scalene"
	}
}

func main() {
	fmt.Println("Triangle Test: v1")
}
